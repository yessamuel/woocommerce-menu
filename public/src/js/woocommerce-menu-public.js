/* eslint-disable no-console */
/**
 * WooCommerce Menu General Class
 *
 */

class WooCommerceMenuPublic {
	constructor() {
		this.button = document.getElementById('wmc_button');
		this.container = document.getElementById('wmc_container');
		this.overlay = document.getElementById('wmc_overlay');
		this.close = document.getElementById('wmc_close');
	}

	init() {
		this.eventListeners();
	}

	eventListeners() {
			this.button.addEventListener('click', e => {
			this.openContainer();
		});

		this.close.addEventListener('click', () => this.closeContainer());
	}


	replaceNode(inputNode, native = false) {
		// w - wrapper
		const iA = document.querySelectorAll(inputNode);
		iA.forEach(i => {
			const w = ( ! native ? i : window.getParents(i, document.querySelector('form').parentNode) );

			while (w.firstChild) {
				w.removeChild(w.firstChild);
			}
			
			this.buildBtn(w);
		
		});
	}

	buildBtn(w) {
		this.nodes.btn = document.createElement('button');
		this.nodes.btn.classList.add('acls-btn');
		// TODO Translate.
		this.nodes.btn.innerHTML = this.inputPlaceholder;

		w.appendChild(this.nodes.btn);
		// eslint-disable-next-line no-undef
		jQuery(this.nodes.btn).fadeIn();

		this.nodes.btn.addEventListener('click', e => {
			if (!this.nodes.container) {
				e.target.blur();
			} else {
				this.nodes.container.classList.add('__showing', '__d-block');
				setTimeout(() => this.openContainer(), this.stTime);
			}
		});
	}

	openContainer() {
		this.overlay.classList.add('wmc_overlay__on');
		this.container.classList.add('wmc_container__on');
	}

	closeContainer() {
		this.overlay.classList.remove('wmc_overlay__on');
		this.container.classList.remove('wmc_container__on');
	}

	request() {
	
	}
}

window.addEventListener('load', () => {
	const s_woocommerce_menu = new WooCommerceMenuPublic();
	s_woocommerce_menu.init();
});

/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
(function (d, w, $) {
	'use strict';

})(document, window, jQuery);

/**
 * Get the parent node for the select element.
 *
 * @param {Object} el The element from where to bubble.
 * @param {Object} parentSelector The parent to look for, if empty bubble to document.
 * @return {Object} Return the parent node.
 */
window.getParents = (el, parentSelector) => {
	if (typeof parentSelector === 'undefined') {
		// eslint-disable-next-line no-param-reassign
		parentSelector = document;
	}

	const parents = [];
	let p = el.parentNode;

	while (p !== parentSelector) {
		const o = p;
		parents.push(o);
		p = o.parentNode;
	}

	return parentSelector;
};
