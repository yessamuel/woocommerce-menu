<?php

/**
 * Fired during plugin deactivation
 *
 * @link       samuelsilvapt
 * @since      1.0.0
 *
 * @package    Woocommerce_Menu
 * @subpackage Woocommerce_Menu/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woocommerce_Menu
 * @subpackage Woocommerce_Menu/includes
 * @author     Samuel Silva <hello@samuelsilva.pt>
 */
class Woocommerce_Menu_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
