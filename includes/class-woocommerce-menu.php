<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       samuelsilvapt
 * @since      1.0.0
 *
 * @package    Woocommerce_Menu
 * @subpackage Woocommerce_Menu/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Woocommerce_Menu
 * @subpackage Woocommerce_Menu/includes
 * @author     Samuel Silva <hello@samuelsilva.pt>
 */
class Woocommerce_Menu {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Woocommerce_Menu_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'WOOCOMMERCE_MENU_VERSION' ) ) {
			$this->version = WOOCOMMERCE_MENU_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'woocommerce-menu';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

		$options = $this->get_options();
		
		if( $options || $options['activate'] ){
			add_action( 'wp_footer', function(){
				global $options;
				$this->construct_woocommerce_menu( $options );
			});
		}

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Woocommerce_Menu_Loader. Orchestrates the hooks of the plugin.
	 * - Woocommerce_Menu_i18n. Defines internationalization functionality.
	 * - Woocommerce_Menu_Admin. Defines all hooks for the admin area.
	 * - Woocommerce_Menu_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-menu-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-woocommerce-menu-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-woocommerce-menu-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-woocommerce-menu-public.php';

		$this->loader = new Woocommerce_Menu_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Woocommerce_Menu_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Woocommerce_Menu_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Woocommerce_Menu_Admin( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$plugin_admin->create_settings_page( $this->get_google_fonts(), $this->get_products() );

	}
	private function get_google_fonts() {

		$google_fonts_name = [
			'Nunito Sans',
			'Amiko',
			'Archivo Black',
			'Roboto',
			'Open Sans',
			'Jomolhari',
			'Bowlby One SC',
			'Lato',
			'Lora',
			'Oswald',
			'Source Sans Pro',
			'Montserrat',
			'Raleway',
			'PT Sans',
			'Prompt',
			'Ubuntu',
			'Work Sans',
		];

		$google_fonts;

		$google_fonts['inherit'] = 'Default Font from your Theme';

		foreach ( $google_fonts_name as $font ) {
			$google_fonts[ $font ] = $font;
		}
		return $google_fonts;

	}

	private function get_products(){
		$args = array(
			'post_type'      => 'product',
			'posts_per_page' => -1,
		);
	
		$loop     = get_posts( $args );
		$products = array();
		foreach( $loop as $product ){
			$products[ $product->ID ] = $product->post_title;
		}
		
		return $products;
	}
	public function get_options(){
		return get_option( 'woocommerce-menu' );
	}
	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	 function define_public_hooks() {

		$plugin_public = new Woocommerce_Menu_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	
	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Woocommerce_Menu_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return  2  string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	public function construct_woocommerce_menu( $options = false ) {
		$options = $this->get_options();

		$style = '';
		//Integrate Google Font
		if( $options['font'] ) {

			$style .= ' 
			@import url("http://fonts.googleapis.com/css2?family=' . esc_attr( $options['font'] ). '&display=swap"); 
			';
			
		}		
		
		//render CSS button
		$style .= $this->render_css_button( $options['icon']['url'], $options['minwitdh'], $options['maxwidth'] );
		$html_button = $this->render_html_button( $options['animation_type'] );
		$style .= $this->render_css_background( $options['backgroundcolor'], $options['backgroundimage']['url'] );
		$style .= $this->render_text_appearance( $options['color'], $options['font'], $options['weight'] );
		// Create html Content
		$html_container = $this->render_welcome_message( $options['welcome_message'] );
		if ( class_exists( 'WooCommerce' ) ) {
			$html_container .= '<div class="wmc_flexi">';
			$html_container    .= '<div class="wmc_featured_container">';
			$html_container      .= '<h3>' . $options['featured_products_message'] . '</h3>';
			$html_container      .= $this->get_featured_products();
			$html_container    .= '</div>';
			$html_container    .= '<div class="wmc_cart_container">';
			$html_container      .= '<h3>' . $options['products_in_cart_message'] . '</h3>';
			$html_container      .= $this->get_products_in_cart( $options['empty_cart'] );
			$html_container    .= '</div>';
			$html_container    .= '<div class="wmc_row_container">';
			$html_container       .= $this->get_total_row();
			$html_container    .= '</div>';
			$html_container .= '</div>';
		}
		$html_container .= $this->get_footer_links( $options['footer_links'] );
		$html_container .= $this->render_close_button();
		$scripts = $this->get_owl_carousel_scripts();


		echo '<style>' . $style . '</style>';
		echo $html_button;
		echo '<div id="wmc_overlay" class="wmc_overlay"></div><div id="wmc_container" class="wmc_container">' . $html_container . '</div>';
		echo $scripts;
		
	}

	public function render_css_button( $icon = false, $minwidth, $maxwidth ){
		if( ! $icon ){
			$icon =  plugin_dir_url( __FILE__ ) . 'img/cart.svg';
		}
		$style = '';
		//media query (hide motive)
		$minwidth = ( $minwidth['width']  > 0 ? ' and (min-width: ' . $minwidth['width']  . $minwidth['unit'] . ')' : '' );
		$maxwidth  = ( $maxwidth['width']  > 0 ? ' and (max-width: ' . $maxwidth['width']  . $maxwidth['unit'] . ')' : '' );
		
		if( $minwidth || $maxwidth ){
			$style = '@media only screen' . $minwidth . $maxwidth . '{
						 .wmc_button{ 
							 display:none; 
		  				  } 
					}';
			}

		$style .= '
		.wmc_button{
			background-image: url(' . $icon . ');
		}
		';

		
		return  $style;
	}

	public function render_css_background( $bgcolor = 'white', $bgimage = '' ){
		$style   = '';
		$bgimage = '';

		if( $bgimage ){
			$bgimage = 'background-image:url(' . $bgimage . ');';
		}
		$style = '.wmc_container{
			background-color: ' . $bgcolor . ';
			' . $bgimage . '
		}';
		return $style;

	}

	public function render_text_appearance( $color = 'grey', $font = false, $weight = '300' ){
		
		$font_family = ( $font ? 'font-family: ' . $font . ';' : '' );
		$style = '';
		
		
		
		$style .= '.wmc_container{
			' . $font_family . '
			color: ' . $color . ';
			font-weight: ' . $weight .';
		}';
			
		return $style;

	}

	public function render_html_button( $animation = '' ){
		
		$html = '<div id="wmc_button" class="wmc_button ' . $animation . '"></div>';
			
		return $html;

	}
	public function render_welcome_message( $welcome_message = false ){
		
		if( ! $welcome_message ){
			return;
		}

		if ( is_user_logged_in()) {
			$user = wp_get_current_user();
			$html = '<p class="wmc_welcome">' . $welcome_message . ', ' . $user->first_name . ' ' . $user->last_name . '.</p>';
		 } else {
			$html = '<p class="wmc_welcome">' . $welcome_message . '.</p>';
		 }
		 return $html;

	}

	public function get_featured_products(){
		$args = array(
			'tax_query' => array(
				array(
					'taxonomy' => 'product_visibility',
					'field'    => 'name',
					'terms'    => 'featured',
				),
			),
		);
		$products_query = wc_get_products( $args );
		$products = $this->render_products( $products_query, true );
		return $products;

	}

	public function get_products_in_cart( $empty_message = '' ){
		
		$cart = WC()->cart->get_cart();
		$products = $this->render_products( $cart, false, $empty_message );
		return $products;

	}

	public function render_products( $products = false, $featured = false, $empty_message = '' ){

		$html     = '<div class="owl-carousel wmc_carousel">';
		if( ! $products || count($products) < 1 ){
			return '<p class="wmc_cart_empty_message">' . $empty_message . '</p>';
		}
		foreach( $products as $product): 
			
			$price = $this->get_woocommerce_price( $product, $featured );

			if( ! $featured ){
				$product = $product['data'];
			}
			$title = $product->get_title();
			$image = $product->get_image();
			$url   = get_the_permalink( $product->get_id() );

			$html .= '<div class="item">
			<a href="' . $url . '">
				<div class="wmc_image">
					' . $image . '
				</div>
				<div class="wmc_title">
					<p>' . $title . '</p>
				</div>'
			. $price .	
			'</a>
		    </div>';
			
		endforeach;
		
		return $html . '</div>';
	}

	public function get_woocommerce_price( $product, $featured ){
		$price    = array();
		$currency = get_woocommerce_currency_symbol();
		$html_price = '';

		if( ! $featured ){
			if( $product['variation_id'] ){
				$product = wc_get_product($product['variation_id']);
			 }else{
				$product = wc_get_product($product['product_id']);   
			 }
		}

		$price['sale']    = ( $product->get_sale_price() ? $currency . $product->get_sale_price() : false );
		$price['regular'] = $currency . $product->get_regular_price();

		if( $featured ){

			if( $price['sale'] ){
				$html_price = '<p class="wmc_regularprice wmc_dashed">' . $price['regular'] . '</p>';
				$html_price .= '<p class="wmc_saleprice">' . $price['sale'] . '</p>';
			} else {
				$html_price = '<p class="wmc_regularprice">' . $price['regular'] . '</p>';
			}

			return $html_price = '<div class="wmc_price">' . $html_price . '</div>';

		} else{
			// if  cart
			$quantities = WC()->cart->get_cart_item_quantities();			
			$cart_qty   = $quantities[ $product->get_id() ];
			$price      = ( $price['sale'] ? $price['sale'] : $price['regular'] );

			return $html_price = '<p class="wmc_regularprice">' . $cart_qty . ' x ' . $price . '</p>';
		}
	


	}
	
	public function get_total_row() {
		global $woocommerce;
		$total        = $woocommerce->cart->get_cart_total();
		$checkout_url = wc_get_checkout_url();
		
		$html = '<div class="wmc_total_row">
			<div class="wmc_total">' . __( 'Total', 'woocommerce-menu' ) . '<span>' . $total . '</span></div>
			<div class="wmc_checkout">
						<a href="' . $checkout_url . '"><div class="wmc_checkout_button">' . __( 'Checkout', 'woocommerce-menu' ) . '</div></a>
				</div>
			</div>';
		return $html;

	}

	public function get_footer_links( $links ) {
		if( ! $links ){
			return;
		}
		$html = '<ul class="wmc_footer_link">';

		foreach( $links as $link ){
			$html .= '<li><a href="' . get_the_permalink( $link ) . '">' . get_the_title( $link ) . '</a></li>';
		}

		$html .= '</ul>';
		return $html;
	}
	
	public function render_close_button(){
		return '<div id="wmc_close" class="wmc_close"></div>';
	}
	public function get_owl_carousel_scripts(){
		return '<script>
		jQuery(document).ready(function(){
			jQuery(".wmc_carousel").owlCarousel({
				nav:true,
				dots:true,
				responsive:{
					0:{
						items:2
					},
					600:{
						items:2
					},
				}
			});
		});
		</script>';
	}
}