=== WooCommerce Menu ===
Contributors: samuelsilvapt
Tags: woocommerce, ecommerce, menu
Requires at least: 5.6
Tested up to: 5.7.1
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin provides a WooCommerce Menu accessed by a Badge on the Bottom right of your website.

== Description ==

The best way to improve the Experience of e-commerce users is to focus on their needs. 
With WooCommerce Menu, is possible to group all the links and important content from an E-commerce environment. With this plugin, you can show:

This plugin is a WooCommerce extension, WooCommerce is a required plugin for the extension works. 

1. A Welcome Message
2. Featured Products (defined on wp-admin)
3. Products in Cart (or a message if the Cart is empty)
4. Cart Totals
5. Checkout Call-to-action
6. Cart and My Account Links

== Installation ==

1. Upload `woocommerce-menu` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to WP-admin->WooCommerce Menu and find the settings panel

== Frequently Asked Questions ==

Not yet.

== Screenshots ==

1. WooCommerce menu opened (default styles)
2. Settings 01
3. Settings 02
4. Settings 03

== Changelog ==

= 0.1 =
* First stable version (not 1.0 yet)
