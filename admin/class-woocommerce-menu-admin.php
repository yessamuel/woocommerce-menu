<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       samuelsilvapt
 * @since      1.0.0
 *
 * @package    Woocommerce_Menu
 * @subpackage Woocommerce_Menu/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Woocommerce_Menu
 * @subpackage Woocommerce_Menu/admin
 * @author     Samuel Silva <hello@samuelsilva.pt>
 */
class Woocommerce_Menu_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Menu_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Menu_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/woocommerce-menu-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Woocommerce_Menu_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Woocommerce_Menu_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/woocommerce-menu-admin.js', array( 'jquery' ), $this->version, false );

	}
	public function get_pages() {
		$pages = get_pages();
		$resul = array();
		foreach( $pages as $page ){
			$resul[ $page->ID ] = $page->post_title;
		}
		return $resul;
	}
	public function create_settings_page( $google_fonts, $products ) {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/vendor/codestar-framework/codestar-framework.php';

		if ( class_exists( 'CSF' ) ) {

			$prefix = $this->plugin_name;

			CSF::createOptions(
				$prefix,
				array(
					'menu_title' => 'WooCommerce Menu',
					'menu_slug'  => 'woocommerce-menu',
					'menu_icon'  => 'dashicons-menu',
					'theme'      => 'light',
				)
			);
			
			CSF::createSection(
				$prefix,
				array(
					'title'  => __( 'General', $prefix ), //phpcs:ignore
					'fields' => array(
						array(
							'id'    => 'activate',
							'type'  => 'checkbox',
							'title' => __( 'Activate WooCommerce Menu', $prefix ), //phpcs:ignore
						),
						/*
						array(
							'id'       => 'anchor',
							'type'     => 'checkbox',
							'title'    => __( 'Close menu on clicking a menu item', $prefix ), //phpcs:ignore
							'subtitle' => __( 'Useful if you are using the menu with anchor links.', $prefix ) //phpcs:ignore
						),
						
						array(
							'id'      => 'type',
							'type'    => 'select',
							'title'   => __( 'Menu type (opened)', $prefix ),
							'options' => array(
								'lateral'    => __( 'Lateral', $prefix ),
								'fullscreen' => __( 'Fullscreen', $prefix ),
							)
						),
						*/
						array(
							'id'      => 'welcome_message',
							'title'   => __( 'Welcome message', $prefix ),
							'default' => __( 'Welcome', $prefix ),
							'type'    => 'text',
						),
						array(
							'id'      => 'featured_products_message',
							'title'   => __( 'Featured Products Message', $prefix ),
							'default' => __( 'Featured Products', $prefix ),
							'type'    => 'text',
						),
						array(
							'id'      => 'products_in_cart_message',
							'title'   => __( 'Products in Cart Message', $prefix ),
							'default' => __( 'Products in Cart', $prefix ),
							'type'    => 'text',
						),
						
						array(
							'id'      => 'freehtml',
							'type'    => 'wp_editor',
							'title'   => __( 'Free HTML / Shortcodes', $prefix ),
							'subtitle' => __( 'This will be added after all the elements')
						),
					),
				)
			);

			CSF::createSection(
				$prefix,
				array(
					'title'  => __( 'Design/Appearance', $prefix ), //phpcs:ignore
					'fields' => array(
							array(
								'type'    => 'subheading',
								'content' => __( 'Responsive Sizes', $prefix ),
							),
							array(
								'id'       => 'minwitdh',
								'type'     => 'dimensions',
								'height'   => false,
								'title'    => __( 'Min Witdh to show the Menu (in pixels)', $prefix ), //phpcs:ignore
								'subtitle' => __( 'Leave black if you want to show at all dimensions', $prefix ), //phpcs:ignore
							),
							array(
								'id'       => 'maxwidth',
								'type'     => 'dimensions',
								'height'   => false,
								'title'    => __( 'Max Witdh to show the Menu (in pixels)', $prefix ), //phpcs:ignore
								'subtitle' => __( 'Leave black if you want to show at all dimensions', $prefix ), //phpcs:ignore
							),
							array(
								'type'     => 'media',
								'id'       => 'icon',
								'type'     => 'media',
								'title'    => __( 'Cart Icon', $prefix ),
								'subtitle' => __( 'Default Icon: ', $prefix ) . '<img src="' . plugin_dir_url( __FILE__ ) . 'img/cart.svg">', //phpcs:ignore

							),
							array(
								'type'    => 'subheading',
								'content' => __( 'Background Image/Color', $prefix ),
							),
							array(
								'id'       => 'backgroundcolor',
								'type'     => 'color',
								'title'    => __( 'Background Menu Color', $prefix ), 
							),
							
							array(
								'id'       => 'backgroundimage',
								'type'     => 'media',
								'title'    => __( 'Background Image', $prefix ), 
								'subtitle' => __( 'Background Image when menu is opened. Leave blank to use colors above.', $prefix ),
							),
							
							array(
								'type'   => 'subheading',
								'content'  => 'Text Appearance',
							),
							array(
								'id'       => 'color',
								'type'     => 'color',
								'title'    => __( 'Font Color', $prefix ), 
							),
							array(
								'id'       => 'font',
								'type'     => 'select',
								'title'    => __( 'Font Family', $prefix ), 
								'options'  => $google_fonts
							),
							array(
								'id'       => 'weight',
								'type'     => 'select',
								'title'    => __( 'Font Weight', $prefix ), 
								'options'  => array(

									'100' => '100',
									'300' => '300',
									'500' => '500',
									'700' => '700',
									'900' => '900',
								),
							),
							array(
								'type'    => 'subheading',
								'content' => 'Animation Settings',
							),
							array(
								'id'       => 'animation_type',
								'type'     => 'select',
								'title'    => __( 'Animation Type', $prefix ),
								'subtitle' => __( 'If select "From Left to Right" option, the icon will be on the left.', $prefix ),
								'options'  => array(
									'opacity' => __( 'Opacity (0 to 100)', $prefix ),
									'left'    => __( 'From Left to Right', $prefix ),
									'right'   => __( 'From Right to Left', $prefix ),

								),
							),
					),
				)
			);

			/*

			CSF::createSection(
				$prefix,
				array(
					'title'  => __( 'Search Bar', $prefix ),
					'fields' => array(
						array(
							'id'    => 'activate_search',
							'title' => __( 'Activate Search Bar', $prefix ),
							'subtitle' => __( 'To search WooCommerce products', $prefix ),
							'type'  => 'checkbox',
						),
						array(
							'id'      => 'search_placeholder',
							'title'   => __( 'Search Input Placeholder', $prefix ),
							'default' => __( 'Search products...', $prefix ),
							'type'    => 'text',
						),
					),
				)
			);
			*/

			CSF::createSection(
				$prefix,
				array(
					'title'  => __( 'WooCommerce Settings', $prefix ),
					'fields' => array(
						array(
							'id'       => 'activate_woocommerce_menu',
							'title'    => __( 'Add WooCommerce Menu', $prefix ),
							'subtitle' => __( 'Adds "My Account", "Shop", "Cart" and "Checkout" menus', $prefix ),
							'type'     => 'checkbox',
						),
						/*
						array(
							'id'       => 'woocommerce_cart',
							'title'    => __( 'Add WooCommerce Cart', $prefix ),
							'subtitle' => __( 'Display a carousel with the products the user has in cart', $prefix ),
							'type'     => 'checkbox',
						),
						*/
						array(
							'id'       => 'empty_cart',
							'title'    => __( 'Empty Cart message', $prefix ),
							'type'     => 'text',
							'subtitle' => __( 'This message will be appear if you have WooCommerce Cart option activated and the user hasn\'t products in cart yet.', $prefix ),
							'default'  => __( 'Your Cart is empty. :( Go see our products or talk with us!', $prefix ),	
						),
						array(
							'id'       => 'featured_products',
							'title'    => __( 'Featured Products', $prefix ),
							'subtitle' => __( 'Select products to highlight in WooCommerce Menu', $prefix ),
							'type'     => 'select',
							'multiple' => true,
							'chosen'   => true,
							'options'  => $products
						)
					),
				)
			);
			CSF::createSection(
				$prefix,
				array(
					'title'  => __( 'Other Content', $prefix ),
					'fields' => array(
						/*
						array(
							'id'     => 'socialmedia',
							'type'   => 'group',
							'title'  => __( 'Social Media Menu', $prefix ),
							'fields' => array(
								array(
									 'id'    => 'title',
									 'title' => 'Title',
									 'type'  => 'text',
								),
								array(
									'id'    => 'url',
									'title' => __( 'Social Url', $prefix ),
									'type'  => 'text',
								),
								array(
									'id'    => 'icon',
									'title' => __( 'Social Icon', $prefix ),
									'type'  => 'icon',
									
								),
							)	
						),
						*/
						array(
							'id'       => 'footer_links',
							'type'     => 'select',
							'title'    => __( 'Footer Links', $prefix ),
							'subtitle' => __( 'Links will appear at the widget\'s footer. Example: Cart, My Account. For design reasons, we suggest select only 2 pages.', $prefix ),
							'options'  => $this->get_pages(),
							'multiple' => true,
							'chosen'   => true
							
						),
					)
				)
			);

		}

	}

}